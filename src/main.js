// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './scss/main.scss'
import Vue from 'vue'
import App from './App'
import router from './router'

import DisplayApp from './components/display'
import InputApp from './components/input'

Vue.component('display-app',DisplayApp);
Vue.component('input-app',InputApp);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { 
    App,
  },
  template: '<App/>'
})
